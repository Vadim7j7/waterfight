require './structurs.rb'
require './help.rb'

class Map
  attr_accessor :buffer, :top_line

  def initialize(size=10)
    symbols = %w'A B C D E F G H I J H'

    self.top_line = []
    self.buffer = []
    (0...size).each do |y|
      self.top_line << symbols[y]
      self.buffer[y] = []
      (0...size).each do |x|
        self.buffer[y][x] = ObjectTypes::NULL
      end
    end
  end

  def get_cell(x=0, y=0)
    self.buffer[x][y]
  end

  def set_cell(x=0, y=0, value=0)
    self.buffer[x][y] = value
  end

  def render
    _render
  end

  private
    def _render
      cls
      puts "   #{self.top_line.join(' ')}"
      n = 1
      self.buffer.each do |row|
        line = []
        row.each do |cell|
          line << ObjectTypes::DISPLAY[cell]
        end
        puts "#{n < 10 ? " #{n}" : n} #{line.join(' ')}"
        n += 1
      end

      puts ' '
    end
end
