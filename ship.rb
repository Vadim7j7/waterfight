require './structurs.rb'

class GenerateShips
  attr_accessor :row_size

  def initialize(row_size=10)
    self.row_size = row_size - 1
    self.block_points = []
    self.ships = []

    gen_ship(4)
    gen_ship(3)
    gen_ship(3)

    gen_ship(2)
    gen_ship(2)
    gen_ship(2)

    gen_ship(1)
    gen_ship(1)
    gen_ship(1)
    gen_ship(1)
  end

  def get_ships
    return ships
  end

  private
    attr_accessor :block_points, :ships

    def gen_point(point)
      # Выбираем правильные координаты куда направиться
      tmp = []
      if point.x > 0 && point.y > 0 && point.x < self.row_size && point.y < self.row_size
        tmp << Point.new(point.x, point.y+1) #Point.new(point.x, point.y+1)
        tmp << Point.new(point.x, point.y-1)
        tmp << Point.new(point.x+1, point.y)
        tmp << Point.new(point.x-1, point.y)
      end
      if point.x == 0 && point.y > 0
        tmp << Point.new(point.x, point.y+1)
        tmp << Point.new(point.x, point.y-1)
        tmp << Point.new(point.x+1, point.y)
      end
      if point.x > 0 && point.y == 0
        tmp << Point.new(point.x, point.y+1)
        tmp << Point.new(point.x+1, point.y)
        tmp << Point.new(point.x-1, point.y)
      end
      if point.x >= self.row_size && point.y < self.row_size
        tmp << Point.new(point.x, point.y+1)
        tmp << Point.new(point.x, point.y-1)
        tmp << Point.new(point.x-1, point.y)
      end
      if point.x < self.row_size && point.y >= self.row_size
        tmp << Point.new(point.x, point.y-1)
        tmp << Point.new(point.x+1, point.y)
        tmp << Point.new(point.x-1, point.y)
      end
      if point.x == 0 && point.y == 0
        tmp << Point.new(point.x+1, point.y)
        tmp << Point.new(point.x, point.y+1)
      end
      if point.x >= self.row_size && point.y >= self.row_size
        tmp << Point.new(point.x-1, point.y)
        tmp << Point.new(point.x, point.y-1)
      end
      if point.x == 0 && point.y >= self.row_size
        tmp << Point.new(point.x, point.y-1)
        tmp << Point.new(point.x+1, point.y)
      end
      if point.x >= self.row_size && point.y == 0
        tmp << Point.new(point.x, point.y+1)
        tmp << Point.new(point.x-1, point.y)
      end

      # Отбрасываем левые варианты
      tmp = tmp.select {|t| !is_blocked_point?(t) }

      return tmp[rand(0...tmp.count)]
    end

    def gen_ship(size_ship)
      size_ship = size_ship - 1
      point = nil
      ship = []
      while true
        point = Point.new(rand(0..self.row_size),rand(0..self.row_size))
        unless is_blocked_point?(point)
          block_points << point
          break
        end
      end
      ship << point

      (0...size_ship).each do |i|
        point = gen_point(point)
        block_points << point
        ship << point
      end

      ships << ship
      add_ship_to_block_points(ship)
    end

    def is_blocked_point?(point)
      block_points.each do |block|
        if block.x == point.x && block.y == point.y
          return true
        end
      end

      return false
    end

    def add_ship_to_block_points(ship)
      ship.each do |cell|
        point1 = Point.new(cell.x-1, cell.y-1)
        point2 = Point.new(cell.x, cell.y-1)
        point3 = Point.new(cell.x+1, cell.y-1)
        point4 = Point.new(cell.x+1, cell.y)
        point5 = Point.new(cell.x+1, cell.y+1)
        point6 = Point.new(cell.x, cell.y+1)
        point7 = Point.new(cell.x-1, cell.y+1)
        point8 = Point.new(cell.x-1, cell.y)

        unless is_blocked_point?(point1)
          block_points << point1
        end
        unless is_blocked_point?(point2)
          block_points << point2
        end
        unless is_blocked_point?(point3)
          block_points << point3
        end
        unless is_blocked_point?(point4)
          block_points << point4
        end
        unless is_blocked_point?(point5)
          block_points << point5
        end
        unless is_blocked_point?(point6)
          block_points << point6
        end
        unless is_blocked_point?(point7)
          block_points << point7
        end
        unless is_blocked_point?(point8)
          block_points << point8
        end
      end
    end

end
