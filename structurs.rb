class ObjectTypes

  NULL = 0
  SHIP = 1
  HIT = 2
  MISS = 3

  DISPLAY = {
      NULL => '.',
      SHIP => 'S',
      HIT => 'X',
      MISS => 'O'}

end

class Point
  attr_accessor :x, :y, :st

  def initialize(x, y, st=ObjectTypes::NULL)
    self.x = x
    self.y = y
    self.st = st
  end

end