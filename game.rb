require './map.rb'
require './ship.rb'

class Game
  attr_accessor :field_size, :map, :g_ship, :ships, :fare_points

  def initialize
    self.field_size = 10

    self.map = Map.new(self.field_size)
    self.g_ship = GenerateShips.new(self.field_size)
  end

  def start
    self.ships = self.g_ship.get_ships
    self.map.render
    self.fare_points = []

    loop_events
  end

  private
    def loop_events
      while user_input = gets.chomp
        if user_input == 'q' || user_input == 'quit'
          cls
          puts 'Goodbye'
          break
        elsif user_input == 'surrender'

          self.ships.each do |ship|
            ship.each do |cell|
              self.map.set_cell(cell.x, cell.y, ObjectTypes::SHIP)
            end
          end

          view_fare_point
          re_render
          puts 'What a shame…'

          next
        elsif user_input == 'return'
          view_reset
          next
        else
          x = user_input[0]
          y = user_input[1..-1]
          if x && y
            x = x.upcase
            y = y.to_i
            x = map.top_line.index(x)
            if y > 0 && y <= self.field_size && x
              point = Point.new(y - 1, x)
              unless is_fare_points?(point)
                fare(point)
                calc_score
              else
                puts 'Sorry but you have already shot here'
              end
              next
            end
          end
        end
        puts 'Invalid command'
      end
    end

    def fare(point)
      value = ObjectTypes::MISS
      self.ships.each do |ship|
        ship.each do |cell|
          if cell.x == point.x && cell.y == point.y
            value = ObjectTypes::HIT
            cell.st = value
            break
          end
        end
      end
      point.st = value
      self.fare_points << point
      self.map.set_cell(point.x, point.y, value)

      re_render
    end

    def is_fare_points?(point)
      self.fare_points.each do |fare_point|
        if fare_point.x == point.x && fare_point.y == point.y
          return true
        end
      end

      return false
    end

    def view_fare_point
      self.fare_points.each do |fare_point|
        self.map.set_cell(fare_point.x, fare_point.y, fare_point.st)
      end
    end

    def view_reset
      self.ships.each do |ship|
        ship.each do |cell|
          self.map.set_cell(cell.x, cell.y, ObjectTypes::NULL)
        end
      end

      view_fare_point
      re_render
    end

    def calc_score
      n = 0
      self.ships.each do |ship|
        ship.each do |cell|
          if cell.st == ObjectTypes::NULL
            n += 1
          end
        end
      end
      if n <= 0
        puts 'You win!'
        exit
      end
    end

    def re_render
      self.map.render
    end
end
